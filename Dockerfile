FROM node:lts-alpine AS build

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install -g @angular/cli
RUN npm install

COPY . .
RUN npm run build

#step 2
FROM nginxinc/nginx-unprivileged

#COPY nginx.conf /etc/nginx/nginx.conf
#COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d/my.conf

COPY --from=build /usr/src/app/dist/camunda-frontend /usr/share/nginx/html               

EXPOSE 8085

CMD ["nginx", "-g", "daemon off;"]




